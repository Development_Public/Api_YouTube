<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Youtube;

class YoutubeController extends Controller
{
    public function index()
    {
    	$videos = Youtube::getActivitiesByChannelId('UC31ZE8dzmokYG6y1ByW158g');
    	foreach ($videos as $key => $video) {
    		if (isset($video->contentDetails->upload)) {
    			$videos[$key]->url = $video->contentDetails->upload->videoId;
    		} else {
    			$videos[$key]->url = $video->contentDetails->playlistItem->resourceId->videoId;
    		}
    	}
//    	return response()->json($video);
    	return view('youtube', array('videos'=>$videos));
    }
}
